package com.cornerbank.tests;

import com.cornerbank.core.BaseTestClass;
import com.cornerbank.core.WebDrivers;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;

public class SimpleHomePageTests extends BaseTestClass {
    @Test
    public void check_home_page_chrome_browser() {
        driver.get("https://www.corner.ch/it/");
        driver.quit();
    }

    @Test
    public void check_home_page_ff_browser() {
        driver.get("https://www.corner.ch/it/");
        driver.quit();
    }
}