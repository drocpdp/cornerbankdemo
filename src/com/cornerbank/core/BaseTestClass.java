package com.cornerbank.core;

import org.openqa.selenium.WebDriver;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;


public class BaseTestClass {

    public static WebDriver driver;

    @BeforeAll
    static void initAll() {
        driver = new WebDrivers().getDriver();
    }


    @AfterAll
    static void tearDownAll() {
        driver.quit();
    }

}
