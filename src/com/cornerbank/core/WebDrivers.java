package com.cornerbank.core;

import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDrivers {

    public WebDriver getDriver(){
        return getDriver(1);
    }

    public WebDriver getDriver(int browserSelection) {
        if (browserSelection == 0){
            return getFirefoxDriver();
        }
        if (browserSelection == 1){
            return getChromeDriver();
        }
        else {
            return getFirefoxDriver();
        }

    }

    @NotNull
    private FirefoxDriver getFirefoxDriver() {
        FirefoxOptions options = new FirefoxOptions()
                .addPreference("browser.startup.page", 1)
                .setAcceptInsecureCerts(true)
                .setHeadless(true);
        WebDriver driver;
        System.setProperty("webdriver.gecko.driver", "/Users/davideynon/Projects/geckodriver");
        driver = new FirefoxDriver(options);
        return (FirefoxDriver) driver;
    }

    @NotNull
    private ChromeDriver getChromeDriver() {
        WebDriver driver;
        driver = new ChromeDriver();
        return (ChromeDriver) driver;
    }
}
