from lib.base_class import BaseClass
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
import time

class BasePage(BaseClass):
	
	def get_elements(self, driver, prop_lookup_key):
		elements = []
		locator_method = self.get_locator_method(prop_lookup_key)
		property_string = self.get_property_lookup_value(prop_lookup_key)

		if locator_method == 'css':
			self.logs('css');
			self.logs(property_string);
			elements = driver.find_elements_by_css_selector(property_string);
		elif locator_method == 'id':
			self.logs('id')
			elements = driver.find_elements_by_id(property_string)
		elif locator_method == 'class':
			self.logs('class')
			elements = driver.find_elements_by_class_name(property_string)
		elif locator_method == 'link':
			self.logs('link')
			elements = driver.find_elements_by_link_text(property_string)
		elif locator_method == 'name':
			self.logs('name')
			elements = driver.find_elements_by_name(property_string)
		elif locator_method == 'partiallink':
			self.logs('partiallink')
			elements = driver.find_elements_by_partial_link_text(property_string)
		elif locator_method == 'tagname':
			self.logs('tagname')
			elements = driver.find_elements_by_tag_name(property_string)
		elif locator_method == 'xpath':
			self.logs('xpath')
			elements = driver.find_elements_by_xpath(property_string)
		return elements;		

	def get_element(self, driver, prop):
		elements = self.get_elements(driver, prop)
		element = elements[0]
		return element

	def is_element_exist(self, driver, prop):
		is_exists = self.get_elements(driver, prop);
		return is_exists;

	def get_locator_method(self, key):
		full_prop_string = self.get_full_property_value(key)
		string_parts = full_prop_string.split("=",1)
		if string_parts[0] == 'css':	
			return 'css'
		elif string_parts[0] == 'class':
			return 'class'
		elif string_parts[0] == 'id':
			return 'id'
		elif string_parts[0] == 'xpath':
			return 'xpath'		
		elif string_parts[0] == 'link':
			return 'link'
		elif string_parts[0] == 'name':
			return 'name'
		elif string_parts[0] == 'partiallink':
			return 'partiallink'
		elif string_parts[0] == 'tagname':			
			return 'tagname'

	def go_to_page(self, driver):
		""" Child class inherits this, calling for its own url property
		"""
		driver.get(self.get_property('url'));

	def get_property_lookup_value(self, key):
		full_prop_string = self.get_full_property_value(key)
		string_parts = full_prop_string.split("=",1)
		return string_parts[1]		

	def is_on_current_page(self, driver):
		"""Blog page (blog_page.py) is the only class that overrides this method"""
		if self.is_element_exist(driver, 'is_on_current_page_indicator_element'):
			expected = self.get_full_property_value('is_on_current_page_indicator_element_value');
			val = self.get_element(driver,'is_on_current_page_indicator_element').text
			self.logs(expected);
			self.logs(val);
			if val == expected:
				return True;
		return False;

	def wait_for_exists(self, driver, prop, timeout=10, poll_frequency=0.25):
		interval = int(timeout / poll_frequency)
		for x in range(0,interval):
			if self.is_element_exist(driver, prop):
				print ('showed');
				return 
			print ('did not show');
			time.sleep(poll_frequency);
		raise NoSuchElementException('Element Not Found');






