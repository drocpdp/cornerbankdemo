import selenium
from selenium.webdriver.chrome.webdriver import WebDriver as ChromeDriver
from lib.base_class import BaseClass

class Driver(BaseClass):

	def get_driver(self, driver=None):
		print ("In Driver().__init__()");
		caps = [];
		if driver is None:
			driver = self.get_chromedriver();
		else:
			driver = driver;
		driver.implicitly_wait(15);
		return driver;

	def get_chromedriver(self):
		driver = ChromeDriver(executable_path="/Users/davideynon/Projects/chromedriver");
		return driver;