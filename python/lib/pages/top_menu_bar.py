import selenium
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from lib.base_page import BasePage
import time


class TopMenuBar(BasePage):

	PROPERTIES_FILE = "top_menu_bar.properties";

	""" Getters """

	def get_top_menu_home_logo_link_object(self, driver):
		return self.get_element(driver, 'top_menu_home_logo_link');

	def get_top_menu_search_icon_link_object(self, driver):
		return self.get_element(driver, 'top_menu_search_icon_link');

	def get_top_menu_corner_group_link_object(self, driver):
		return self.get_element(driver, 'top_menu_corner_group_link');

	def get_top_menu_corner_card_link_object(self, driver):
		return self.get_element(driver, 'top_menu_corder_card_link');

	def get_top_menu_corner_trader_link_object(self, driver):
		return self.get_element(driver, 'top_menu_corner_trader_link');

	def get_top_menu_contact_link_object(self, driver):
		return self.get_element(driver, 'top_menu_contact_link');

	def get_top_menu_language_dropdown_link_object(self, driver):
		return self.get_element(driver, 'top_menu_language_dropdown_link');

	def get_top_menu_language_dropdown_languages_selection_en(self, driver):
		return self.get_element(driver, 'top_menu_language_bar_selection_en');

	def get_top_menu_language_dropdown_languages_selection_french(self, driver):
		""" list of language support options """
		return self.get_element(driver, 'top_menu_language_bar_selection_fr');	

	def get_top_menu_language_dropdown_languages_selection_italian(self, driver):
		""" list of language support options """
		return self.get_element(driver, 'top_menu_language_bar_selection_it');				

	def get_top_menu_language_dropdown_languages_selection_deutsch(self, driver):
		""" list of language support options """
		return self.get_element(driver, 'top_menu_language_bar_selection_de');				

	def get_top_menu_sign_in_link_object(self, driver):
		return self.get_element(driver, 'top_menu_sign_in_link');

	""" Setters / Actions"""

	def click_top_menu_language_dropdown_link_object(self, driver):
		""" Not a traditional dropdown per se """
		self.get_top_menu_language_dropdown_link_object(driver).click()

	def set_language_to_french(self, driver):
		""" passthrough to set_top_menu_language_dropdown_language() """
		self.set_top_menu_language_dropdown_language(driver, 'fr');

	def set_language_to_italian(self, driver):
		""" passthrough to set_top_menu_language_dropdown_language() """
		self.set_top_menu_language_dropdown_language(driver, 'it');		

	def set_language_to_english(self, driver):
		""" passthrough to set_top_menu_language_dropdown_language() """
		self.set_top_menu_language_dropdown_language(driver, 'en');		

	def set_language_to_deutsch(self, driver):
		""" passthrough to set_top_menu_language_dropdown_language() """
		self.set_top_menu_language_dropdown_language(driver, 'de');		

	def set_top_menu_language_dropdown_language(self, driver, language='en'):
		self.click_top_menu_language_dropdown_link_object(driver);
		if (language == 'fr'):
			self.get_top_menu_language_dropdown_languages_selection_french(driver).click();
		if (language == 'it'):
			self.get_top_menu_language_dropdown_languages_selection_italian(driver).click();
		if (language == 'en'):
			self.get_top_menu_language_dropdown_languages_selection_english(driver).click();
		if (language == 'de'):
			self.get_top_menu_language_dropdown_languages_selection_deutsch(driver).click();

	"""Helper Functions"""

	def is_all_top_menu_bar_items_exist(self, driver):
		menu_bar_items = ['top_menu_home_logo_link',
							'top_menu_search_icon_link',
							'top_menu_corner_group_link',
							'top_menu_corder_card_link',
							'top_menu_corner_trader_link',
							'top_menu_contact_link',
							'top_menu_language_dropdown_link',
							'top_menu_sign_in_link'];
		for item in menu_bar_items:
			if self.is_element_exist(driver, item) is False:
				return False;
		return True;

	def get_all_menu_bar_item_text_actual(self, driver, language='en'):
		"""passthrough to get_all_menu_bar_item_text_actual_and_expected()"""
		return self.get_all_menu_bar_item_text_actual_and_expected(driver, language, actual=True);

	def get_all_menu_bar_item_text_expected(self, language='en'):
		"""passthrough to get_all_menu_bar_item_text_actual_and_expected()"""
		return self.get_all_menu_bar_item_text_actual_and_expected(language=language, actual=False);

	def get_all_menu_bar_item_text_actual_and_expected(self, driver=None, language='en', actual=True):
		menu_bar_items = ['top_menu_corner_group_link',
							'top_menu_corder_card_link',
							'top_menu_corner_trader_link',
							'top_menu_contact_link'];
		items = [];
		for item in menu_bar_items:
			if actual:
				items.append(self.get_element(driver, item).text);
			else:
				items.append(self.get_full_property_value(item, language));
		return items;
