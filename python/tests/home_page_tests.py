from python.lib.base_test_class import BaseTestClass
from python.lib.pages.home_page import HomePage
from python.lib.pages.top_menu_bar import TopMenuBar
import time

class HomePageTests(BaseTestClass):

	def test_home_page_test_existence_of_top_menu_bar_links(self):
		home_page = HomePage();
		home_page.go_to_page(self.driver);
		self.assertTrue(TopMenuBar().is_all_top_menu_bar_items_exist(self.driver));

	def test_home_page_test_check_text_of_top_menu_bar_links_english(self):
		home_page = HomePage();
		home_page.go_to_page(self.driver);
		actual = TopMenuBar().get_all_menu_bar_item_text_actual(self.driver, language='en');
		expected = TopMenuBar().get_all_menu_bar_item_text_expected(language='en');
		self.assertListEqual(actual, expected);
	
	def test_home_page_test_check_text_of_top_menu_bar_links_italian(self):
		home_page = HomePage();
		home_page.go_to_page(self.driver);
		TopMenuBar().set_language_to_italian(self.driver);
		actual = TopMenuBar().get_all_menu_bar_item_text_actual(self.driver, language='it');
		expected = TopMenuBar().get_all_menu_bar_item_text_expected(language='it');
		self.assertListEqual(actual, expected);		
	
	def test_home_page_test_check_text_of_top_menu_bar_links_tedesco(self):
		home_page = HomePage();
		home_page.go_to_page(self.driver);
		TopMenuBar().set_language_to_deutsch(self.driver);
		actual = TopMenuBar().get_all_menu_bar_item_text_actual(self.driver, language='de');
		expected = TopMenuBar().get_all_menu_bar_item_text_expected(language='de');
		self.assertListEqual(actual, expected);			

	def test_home_page_test_check_text_of_top_menu_bar_links_francese(self):
		home_page = HomePage();
		home_page.go_to_page(self.driver);
		TopMenuBar().set_language_to_french(self.driver);
		actual = TopMenuBar().get_all_menu_bar_item_text_actual(self.driver, language='fr');
		expected = TopMenuBar().get_all_menu_bar_item_text_expected(language='fr');
		self.assertListEqual(actual, expected);					
